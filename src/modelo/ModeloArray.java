package modelo;

import vista.Salida;

/**
 * Fichero: ModeloArray.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 23-dic-2014
 */
public class ModeloArray implements Modelo{
	
	final static int totalAlumnos = 100;
	Alumno alumnos[];
	
	public ModeloArray(){
		alumnos = new Alumno[totalAlumnos];
	}
	
	@Override
	public void create(Alumno alumno) {
		int idLibre = this.getId();
		alumno.setId(idLibre);
		alumnos[idLibre] = alumno;
	}

	@Override
	public void read() {
		Salida.mostraTitulo("Listado de alumnos desde Vector");
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				Salida.mostrarAlumno(alumnos[i]);
			} else {
				//break;
				//no hacemos nada por si hemos borrado algun sitio y hay mas alumnos despues
			}
		}
	}

	@Override
	public void update(Alumno alumno) {
		try{
			alumnos[alumno.getId()] = alumno;
		} catch(ArrayIndexOutOfBoundsException e) {
			Salida.mostrarError("El id del alumno se encuentra fuera del rango permitido.");
		}
	}

	@Override
	public void delete(Alumno alumno) {
		try{
			alumnos[alumno.getId()] = null;
		} catch(ArrayIndexOutOfBoundsException e) {
			Salida.mostrarError("El id del alumno se encuentra fuera del rango permitido.");
		}
	}

	@Override
	public int getId() {
		int count = 0;
		for (int i=0;i<alumnos.length;i++) {
			count = i;
			if(alumnos[i]==null) {
				break;
			}
			//System.out.println(alumnos[i]);
		}
		return count;
	}
	
}
