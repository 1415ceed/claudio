package modelo;

import java.util.HashSet;
import java.util.Iterator;
import vista.Salida;

/**
 * Fichero: ModeloHashSet.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 26-dic-2014
 */
public class ModeloHashSet implements Modelo{
	
	private HashSet alumnos;
	private int id;
	
	public ModeloHashSet(){
		id=0;
		alumnos = new HashSet();
	}
	
	@Override
	public void create(Alumno alumno) {
		alumnos.add(alumno);
		id++;
	}

	@Override
	public void read() {
		Salida.mostraTitulo("Listado de alumnos desde HashSet");
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno alumno = (Alumno) i.next();
			Salida.mostrarAlumno(alumno);
		}
	}

	@Override
	public void update(Alumno alumno) {
		boolean actualiza = false;
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno al = (Alumno) i.next();
			if(al.getId()==alumno.getId()) {
				//actualiza = true;
				actualiza = alumnos.remove(al);
				alumnos.add(alumno);
				id++;
				break;
			}
		}
		if(!actualiza) {
			Salida.mostrarError("No se ha actualizado ningun registro.");
		}
	}

	@Override
	public void delete(Alumno alumno) {
		boolean actualiza = false;
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno al = (Alumno) i.next();
			if(al.getId()==alumno.getId()) {
				//actualiza = true;
				actualiza = alumnos.remove(al);
				break;
			}
		}
		if(!actualiza) {
			Salida.mostrarError("No se ha eliminado ningun registro.");
		}
	}

	@Override
	public int getId() {
		return id;
			
	}

}
