package modelo;

/**
 * Fichero: Modelo.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public interface Modelo {
	public void create(Alumno alumno); //crear alumno
	public void read(); //muestra los alumnos
	public void update(Alumno alumno); //actualiza el alumno
	public void delete(Alumno alumno); //borra el alumno
	public int getId(); //obtiene el ultimo id dado. usado para create.
}
