package modelo;

import java.util.ArrayList;
import java.util.Iterator;
import vista.Salida;

/**
 * Fichero: ModeloArrayList.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 26-dic-2014
 */
public class ModeloArrayList implements Modelo{
	
	private ArrayList<Alumno> alumnos;
	
	public ModeloArrayList(){
		alumnos = new ArrayList<Alumno>();
	}

	@Override
	public void create(Alumno alumno) {
		alumnos.add(alumno.getId(), alumno);
	}

	@Override
	public void read() {
		Salida.mostraTitulo("Listado de alumnos desde ArrayList");
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno alumno = (Alumno) i.next();
			Salida.mostrarAlumno(alumno);
		}
	}

	@Override
	public void update(Alumno alumno) {
		boolean actualiza = false;
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno al = (Alumno) i.next();
			if(al.getId()==alumno.getId()) {
				//actualiza = true;
				actualiza = alumnos.remove(al);
				alumnos.add(alumno.getId(), alumno);
				break;
			}
		}
		if(!actualiza) {
			Salida.mostrarError("No se ha actualizado ningun registro.");
		}
	}

	@Override
	public void delete(Alumno alumno) {
		boolean actualiza = false;
		Iterator i = alumnos.iterator();
		while(i.hasNext()){
			Alumno al = (Alumno) i.next();
			if(al.getId()==alumno.getId()) {
				//actualiza = true;
				actualiza = alumnos.remove(al);
				break;
			}
		}
		if(!actualiza) {
			Salida.mostrarError("No se ha eliminado ningun registro.");
		}
	}

	@Override
	public int getId() {
		int i=0;
		Alumno alumno;
		Iterator iter = alumnos.iterator();
		while(iter.hasNext()){
			alumno = (Alumno) iter.next();
			if(i!= alumno.getId()) {
				return i;
			}
			i++;
		}
		return i;
			
	}

}
