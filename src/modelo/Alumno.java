package modelo;

/**
 * Fichero: Alumno.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Alumno {
	private int id,edad;
	private String nombre,email;
	
	public Alumno(int id){
		//vacio
		this.id = id;
	}
	
	public Alumno(int id, String nombre, int edad, String email) {
		this.id = id;
		this.nombre = nombre;
		this.edad = edad;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
