package controlador;

import modelo.Alumno;
import modelo.Modelo;
import vista.Vista;

/**
 * Fichero: Controlador.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Controlador {
	
	private Modelo modelo;
	private Vista vista;
	
	public Controlador(Modelo m, Vista v){
		this.modelo = m;
		vista = v;
		
		while(modelo==null) {
			modelo = vista.mostrarMenuModelos();
			break;
		}
		
		this.procesarDatos();
	}
	
	private void procesarDatos(){
		boolean salida = false;
		String opcion;
		Alumno alumno;
		int id;
		while(!salida) {
			opcion = vista.mostrarMenuDatos();
			switch (opcion){
				case "c":
					alumno = vista.obtenerAlumno(modelo.getId());
					modelo.create(alumno);
					break;
				case "r":
					modelo.read();
					break;
				case "u":
					id = vista.pideIdAlumno("u");
					alumno = vista.obtenerAlumno(id);
					modelo.update(alumno);
					break;
				case "d":
					id = vista.pideIdAlumno("d");
					alumno = new Alumno(id);
					modelo.delete(alumno);
			}
		}
	}

}
