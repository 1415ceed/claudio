package controlador;

import modelo.Modelo;
import vista.Vista;

/**
 * Fichero: Main.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO la logica del programa va aqui
		Modelo m = null;
		Vista v = new Vista();
		Controlador c = new Controlador(m, v);
    } 

}
