package vista;

import java.io.IOException;
import modelo.Alumno;
import modelo.Modelo;
import modelo.ModeloArray;
import modelo.ModeloArrayList;
import modelo.ModeloHashSet;

/**
 * Fichero: Vista.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Vista {
	
	public void limpiarConsola() throws IOException {
		String os = System.getProperty("os.name");
		if (os.contains("Windows")) {
			Runtime.getRuntime().exec("cls");
		} else {
			Runtime.getRuntime().exec("clear");
		}
	}
	
	public Modelo mostrarMenuModelos(){
		
		Modelo modelo = null;
		Salida.mostraTitulo(" Menu Estructura");
		Salida.mostrarOpcion("Exit");
		Salida.mostrarOpcion("Vector");
		Salida.mostrarOpcion("ArrayList");
		Salida.mostrarOpcion("s".charAt(0), "HashSet");
		
		boolean opcioninvalida = false;
		char valor;
		do {
			Salida.mostrarSeleccion(opcioninvalida);
			valor = Entrada.pideChar();
			//valorOpcion = valor.
			switch (String.valueOf(valor)){
				case "e" :
				case "E" :
					Salida.salir();
					System.exit(0);
					break;
				case "v" :
				case "V" :
					modelo = new ModeloArray();
					break;
				case "a":
				case "A":
					modelo = new ModeloArrayList();
					break;
				case "s":
				case "S":
					modelo = new ModeloHashSet();
					break;
				
				
			}
			opcioninvalida = true;
		} while (modelo == null);
		
		try {
			this.limpiarConsola();
		} catch (IOException ex) {
			// si no borra consola, no hacemos nada.
		}
		return modelo;
	}
	
	public String mostrarMenuDatos(){
		Salida.mostraTitulo(" Menu crud");
		Salida.mostrarOpcion("e".charAt(0), "Exit");
		Salida.mostrarOpcion("c".charAt(0), "Create");
		Salida.mostrarOpcion("r".charAt(0), "Read");
		Salida.mostrarOpcion("u".charAt(0), "Update");
		Salida.mostrarOpcion("d".charAt(0), "Delete");
		
		boolean opcioninvalida = false;
		boolean opcion = true;
		char valor;
		String retorno = "";
		do {
			Salida.mostrarSeleccion(opcioninvalida);
			valor = Entrada.pideChar();
			//valorOpcion = valor.
			switch (String.valueOf(valor)){
				case "e" :
				case "E" :
					Salida.salir();
					System.exit(0);
					break;
				case "c" :
				case "C" :
					opcion = false;
					retorno = "c";
					break;
				case "r" :
				case "R" :
					opcion = false;
					retorno = "r";
					break;
				case "u" :
				case "U" :
					opcion = false;
					retorno = "u";
					break;
				case "d" :
				case "D" :
					opcion = false;
					retorno = "d";
					break;
			}
			opcioninvalida = true;
		} while (opcion);
		
		return retorno;

	}
	
	public Alumno obtenerAlumno(int id){
		Alumno alumno;
		System.out.println("Ingrese el nombre del alumno:");
		String nombre = Entrada.pideString();
		System.out.println("Edad:");
		int edad = Entrada.pideInt();
		System.out.println("Email:");
		String email = Entrada.pideEmail();
		
		alumno = new Alumno(id,nombre,edad,email);
		
		return alumno;
	}
	
	public int pideIdAlumno(String d) {
		int id;
		if(d.equals("u")){
			System.out.println("Ingrese el ID del usuario a modificar");
			return Entrada.pideInt();
		} else if(d.equals("d")){
			System.out.println("Ingrese el ID del usuario a eliminar");
			return Entrada.pideInt();
		}
		//no debe llegar aqui, si llega debe dar error en el modelo.
		return -1;
	}

}
