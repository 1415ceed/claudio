package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Entrada.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Entrada {
	public static String pideString(){
		InputStreamReader entrada = new InputStreamReader(System.in);
		BufferedReader lectura = new BufferedReader(entrada);
		String mensajeError = "No se ha podido extraer un string. Ingrese nuevamente:";
		String cadena;
		try{
			cadena = lectura.readLine();
			return cadena;
		} catch (IOException e) {
			System.out.println();
			return Entrada.pideString();
		}
		
	}
	
	public static int pideInt(){
		InputStreamReader entrada = new InputStreamReader(System.in);
		BufferedReader lectura = new BufferedReader(entrada);
		String cadena;
		String mensajeError = "No se ha podido extraer un numero. Ingrese nuevamente un numero:";
		int numero=0;
		try{
			cadena = lectura.readLine();
			numero = Integer.parseInt(cadena);
			return numero;
		} catch (IOException e) {
			System.out.println(mensajeError);
			numero = Entrada.pideInt();
			return numero;
		} catch (NumberFormatException e) {
			System.out.println(mensajeError);
			return Entrada.pideInt();
		}
		
	}
	
	public static char pideChar() {
		InputStreamReader entrada = new InputStreamReader(System.in);
		BufferedReader lectura = new BufferedReader(entrada);
		String mensajeError ="No se ha podido extraer un caracter. Ingrese nuevamente un caracter:";
		String cadena;
		try{
			try {
				cadena = lectura.readLine();
			} catch (IOException ex) {
				cadena = " ";
			}
			return cadena.charAt(0);
		} catch (StringIndexOutOfBoundsException s) {
			System.out.println(mensajeError);
			cadena= " ";
			return cadena.charAt(0);
		}
		
	}
	
	public static String pideEmail() {
		String email;
		String mensajeError = "El email no es válido. Ingrese un email válido:";
		
		email = Entrada.pideString();
		Pattern patron = Pattern.compile("^([a-zA-Z0-9])+([\\w-.])?[@]{1}(\\w)+([.]{1}[\\w]{2,3}){1}([.]{1}[\\w]{2}){0,1}");
		Matcher encaja = patron.matcher(email);
		
		if(encaja.find()){
			//System.out.println("Email valido");
			//System.out.println(encaja.find());
			return email;
		} else {
			System.out.println(mensajeError);
			return Entrada.pideEmail();
		}
		
	}
	
	
}
