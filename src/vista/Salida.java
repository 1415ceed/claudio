package vista;

import modelo.Alumno;

/**
 * Fichero: Salida.java
 * @author Claudio <claudio.lonco@gmail.com>
 * @date 14-dic-2014
 */
public class Salida {
	public static void mostraTitulo(String t) {
		System.out.println();//linea en blanco de separacion
		System.out.println("  "+t.toUpperCase());
	}
	
	public static void mostrarOpcion(char c, String t) {
		System.out.println(c + " - " + t);
	}
	
	public static void mostrarOpcion(String t) {
		System.out.println(t.toLowerCase().charAt(0) + " - " + t);
	}
	
	public static void mostrarSeleccion(boolean invalida){
		if(invalida) {
			System.out.println("Ha seleccionado una opción incorrecta. Elija nuevamente:");
		} else {
			System.out.println("Opción:");
		}
	}
	
	public static void mostrarError(String text) {
		System.out.println("  ERROR  !! " + text);
	}
	
	public static void salir() {
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Cerrando el programa.");
	}
	
	public static void mostrarAlumno(Alumno alumno) {
		//System.out.println(alumno.getId()+"\t\t - "+ alumno.getNombre()+"\t\t - " + alumno.getEdad() + "\t\t - " + alumno.getEmail());
		System.out.println("ID: " + alumno.getId());
		System.out.println("Nombre: "+ alumno.getNombre());
		System.out.println("Edad: "+alumno.getEdad());
		System.out.println("Email: "+alumno.getEmail());
		//System.out.println("ID:" + alumno.getId());
	}
}
